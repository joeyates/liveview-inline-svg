# PhoenixLiveViewInlineSvg

This project is a proof of concept of a Phoenix LiveView
(in HTML) with embedded SVG.

The SVG shows the current GMT time and is updated once a second
via a socket push.
