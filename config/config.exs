use Mix.Config

config :phoenix_live_view_inline_svg,
  ecto_repos: [PhoenixLiveViewInlineSvg.Repo]

config :phoenix_live_view_inline_svg, PhoenixLiveViewInlineSvgWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Jf8Yi31S1bpc8WX/PnmmJu7AtDnTwDgyBgAn+EtnMI5eJjyBzR0k4dM0+A156nnq",
  render_errors: [
    view: PhoenixLiveViewInlineSvgWeb.ErrorView, accepts: ~w(html json)
  ],
  pubsub: [name: PhoenixLiveViewInlineSvg.PubSub, adapter: Phoenix.PubSub.PG2],
  live_reload: [
    patterns: [~r{lib/phoenix_live_view_inline_svg_web/live/.*(ex)$}]
  ],
  live_view: [
    signing_salt: "YOUR_SECRET"
  ]

config :phoenix,
  template_engines: [leex: Phoenix.LiveView.Engine]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :phoenix, :json_library, Jason

import_config "#{Mix.env()}.exs"
