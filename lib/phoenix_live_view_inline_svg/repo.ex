defmodule PhoenixLiveViewInlineSvg.Repo do
  use Ecto.Repo,
    otp_app: :phoenix_live_view_inline_svg,
    adapter: Ecto.Adapters.Postgres
end
