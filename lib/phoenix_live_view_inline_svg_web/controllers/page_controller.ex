defmodule PhoenixLiveViewInlineSvgWeb.PageController do
  use PhoenixLiveViewInlineSvgWeb, :controller
  alias Phoenix.LiveView

  def index(conn, _params) do
    LiveView.Controller.live_render(
      conn, PhoenixLiveViewInlineSvgWeb.SvgTickerView, session: %{}
    )
  end
end
