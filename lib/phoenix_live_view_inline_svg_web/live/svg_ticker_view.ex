defmodule PhoenixLiveViewInlineSvgWeb.SvgTickerView do
  use Phoenix.LiveView

  def render(assigns) do
    ~L"""
    <div class="">
      <div>
        <div>
          <button phx-click="more">More!</button>
        </div>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
          viewBox="0 0 300 380"
          preserveAspectRatio="xMidYMid slice"
          style="width: 300px; height: 460px; z-index:-1;"
        >
          <%= for index <- (1 .. @count) do %>
            <text x="50" y="<%= 40 * index %>" font-size="30">
              <%= @time %>
            </text>
          <% end %>
        </svg>
      </div>
    </div>
    """
  end

  def mount(_session, socket) do
    if connected?(socket), do: :timer.send_interval(1000, self(), :update)

    socket = assign(socket, count: 1)
    {:ok, set_time(socket)}
  end

  def handle_info(:update, socket) do
    {:noreply, set_time(socket)}
  end

  def handle_event("more", _value, socket) do
    {:noreply, assign(socket, count: socket.assigns.count + 1)}
  end

  defp set_time(socket) do
    assign(socket, time: now())
  end

  defp now do
    Time.to_iso8601(Time.utc_now())
  end
end
